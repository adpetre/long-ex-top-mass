colors = {
	"TTto2L2Nu"  : "#e42536",
	"TTtoLNu2Q" : "#f89c20",
	"Diboson" :  "#964a8b",
	"WJets" : "#9c9ca1",
	"tW" : "#7a21dd",
	"DY" : "#5790fc",
	"MuonEG" : 1,
	"Muon" : 1,
	"EGamma" : 1,
}
	
labels = {	
	"TTto2L2Nu" : "t#bar{t}(2l)",
	"TTtoLNu2Q" : "t#bar{t}(1l)",
	"Diboson" :  "Diboson",
	"WJets" : "W", 
	"tW" : "Single top", 
	"DY" : "DY", 
	"MuonEG" : "Data", 
	"Muon" : "Data", 
	"EGamma" : "Data", 
}

cross_sections = {
	"TTto2L2Nu"  : 97.4488, 
	"TTtoLNu2Q"  : 403.2549, 
	"WW" : 12.98,      
	"WZto3LNu" : 5.31,      
	"WZto2L2Q" : 8.17,      
	"ZZto4L" : 1.65,      
	"ZZto2L2Nu" : 8.08,      
	"ZZto2L2Q" : 1.19,      
	"WJets" : 64481.58, 
	"tW" : 4.6511,
	"DY" : 6345.99,      
	"DYLowMass" : 19982.5,      
}

# group processes together for plotting
process_groups = {
	"WW" : "Diboson",
}

luminosities = {
	"2022" : 8077, 
	"2022EE" : 27007,      
}

# medium b-tagging working points for robustParticleTransformer from https://btv-wiki.docs.cern.ch/ScaleFactors/Run3Summer22/
wp_btag_transformer_medium = {
	"2022": 0.4319,
	"2022EE": 0.451,
}

# sources of systematic uncertainties on jet energy corrections
jec_variations = [
	"AbsoluteMPFBias",
	# "AbsoluteScale",
	# "AbsoluteStat",
	# "FlavorQCD",
	# "Fragmentation",
	# "PileUpDataMC",
	# "PileUpPtBB",
	# "PileUpPtEC1",
	# "PileUpPtEC2",
	# "PileUpPtHF",
	# "PileUpPtRef",
	# "RelativeFSR",
	# "RelativeJEREC1",
	# "RelativeJEREC2",
	# "RelativeJERHF",
	# "RelativePtBB",
	# "RelativePtEC1",
	# "RelativePtEC2",
	# "RelativePtHF",
	# "RelativeBal",
	# "RelativeSample",
	# "RelativeStatEC",
	# "RelativeStatFSR",
	# "RelativeStatHF",
	# "SinglePionECAL",
	# "SinglePionHCAL",
	# "TimePtEta",
]


filepaths = {
	"2022": {
		"TTto2L2Nu": [
			"/store/mc/Run3Summer22NanoAODv12/TTto2L2Nu_TuneCP5_13p6TeV_powheg-pythia8/NANOAODSIM/130X_mcRun3_2022_realistic_v5-v2/",
		],
		"TTtoLNu2Q": [
			"/store/mc/Run3Summer22NanoAODv12/TTtoLNu2Q_TuneCP5_13p6TeV_powheg-pythia8/NANOAODSIM/130X_mcRun3_2022_realistic_v5-v2/",
		],
		"WW": [
			"/store/mc/Run3Summer22NanoAODv12/WWto2L2Nu_TuneCP5_13p6TeV_powheg-pythia8/NANOAODSIM/130X_mcRun3_2022_realistic_v5-v2/",
		],
		"WJets" : [
			"/store/mc/Run3Summer22NanoAODv12/WtoLNu-2Jets_TuneCP5_13p6TeV_amcatnloFXFX-pythia8/NANOAODSIM/130X_mcRun3_2022_realistic_v5-v2/"
		],
		"tW" : [
			"/store/mc/Run3Summer22NanoAODv12/TWminusto2L2Nu_TuneCP5_13p6TeV_powheg-pythia8/NANOAODSIM/130X_mcRun3_2022_realistic_v5-v2/",
			"/store/mc/Run3Summer22NanoAODv12/TbarWplusto2L2Nu_TuneCP5_13p6TeV_powheg-pythia8/NANOAODSIM/130X_mcRun3_2022_realistic_v5-v2/"
		],
		"DY" : [
			"/store/mc/Run3Summer22NanoAODv12/DYto2L-2Jets_MLL-50_TuneCP5_13p6TeV_amcatnloFXFX-pythia8/NANOAODSIM/130X_mcRun3_2022_realistic_v5-v2/",
		],
		"MuonEG" : [
			"/store/data/Run2022C/MuonEG/NANOAOD/22Sep2023-v1/",
			"/store/data/Run2022D/MuonEG/NANOAOD/22Sep2023-v1/",
		],
		"Muon" : [
			"/store/data/Run2022C/SingleMuon/NANOAOD/22Sep2023-v1/",
			"/store/data/Run2022C/Muon/NANOAOD/22Sep2023-v1/",
			"/store/data/Run2022D/Muon/NANOAOD/22Sep2023-v1/",		
		],
		"EGamma" : [
			"/store/data/Run2022C/EGamma/NANOAOD/22Sep2023-v1/",
			"/store/data/Run2022D/EGamma/NANOAOD/22Sep2023-v1/",		
		],
	},
	"2022EE": {
		"TTto2L2Nu": [
			"/store/mc/Run3Summer22EENanoAODv12/TTto2L2Nu_TuneCP5_13p6TeV_powheg-pythia8/NANOAODSIM/130X_mcRun3_2022_realistic_postEE_v6-v2/",
		],
		"TTtoLNu2Q": [
			"/store/mc/Run3Summer22EENanoAODv12/TTtoLNu2Q_TuneCP5_13p6TeV_powheg-pythia8/NANOAODSIM/130X_mcRun3_2022_realistic_postEE_v6-v2/",
		],
		"WW": [
			"/store/mc/Run3Summer22EENanoAODv12/WWto2L2Nu_TuneCP5_13p6TeV_powheg-pythia8/NANOAODSIM/130X_mcRun3_2022_realistic_postEE_v6-v2",
		],
		"WJets" : [
			"/store/mc/Run3Summer22EENanoAODv12/WtoLNu-2Jets_TuneCP5_13p6TeV_amcatnloFXFX-pythia8/NANOAODSIM/130X_mcRun3_2022_realistic_postEE_v6-v2/",
		],
		"tW" : [
			"/store/mc/Run3Summer22EENanoAODv12/TWminusto2L2Nu_TuneCP5_13p6TeV_powheg-pythia8/NANOAODSIM/130X_mcRun3_2022_realistic_postEE_v6-v2/",
			"/store/mc/Run3Summer22EENanoAODv12/TbarWplusto2L2Nu_TuneCP5_13p6TeV_powheg-pythia8/NANOAODSIM/130X_mcRun3_2022_realistic_postEE_v6-v2/"
		],
		"DY" : [
			"/store/mc/Run3Summer22EENanoAODv12/DYto2L-2Jets_MLL-50_TuneCP5_13p6TeV_amcatnloFXFX-pythia8/NANOAODSIM/130X_mcRun3_2022_realistic_postEE_v6-v2/",
		],
		"MuonEG_E" : [
			"/store/data/Run2022E/MuonEG/NANOAOD/22Sep2023-v1/",
		],
		"MuonEG_F" : [
			"/store/data/Run2022F/MuonEG/NANOAOD/22Sep2023-v1/",
		],
		"MuonEG_G" : [
			"/store/data/Run2022G/MuonEG/NANOAOD/22Sep2023-v1/",
		],
		"Muon_E" : [
			"/store/data/Run2022E/Muon/NANOAOD/22Sep2023-v1/",
		],
		"Muon_F" : [
			"/store/data/Run2022F/Muon/NANOAOD/22Sep2023-v2/",
		],
		"Muon_G" : [
			"/store/data/Run2022G/Muon/NANOAOD/22Sep2023-v1/",		
		],
		"EGamma_E" : [
			"/store/data/Run2022E/EGamma/NANOAOD/22Sep2023-v1/",
		],
		"EGamma_F" : [
			"/store/data/Run2022F/EGamma/NANOAOD/22Sep2023-v1/",
		],
		"EGamma_G" : [
			"/store/data/Run2022G/EGamma/NANOAOD/22Sep2023-v2/",		
		],
	}
}

filepaths_systematics = {
	"2022": {
		"TTto2L2Nu_mtop1695" : [ 
			"/store/mc/Run3Summer22NanoAODv12/TTto2L2Nu_MT-169p5_TuneCP5_13p6TeV_powheg-pythia8/NANOAODSIM/130X_mcRun3_2022_realistic_v5-v2/",
		],
		"TTto2L2Nu_mtop1755" : [ 
			"/store/mc/Run3Summer22NanoAODv12/TTto2L2Nu_MT-175p5_TuneCP5_13p6TeV_powheg-pythia8/NANOAODSIM/130X_mcRun3_2022_realistic_v5-v2/",
		],
		"TTto2L2Nu_Hdamp158" : [ 
			"/store/mc/Run3Summer22NanoAODv12/TTto2L2Nu_Hdamp-158_TuneCP5_13p6TeV_powheg-pythia8/NANOAODSIM/130X_mcRun3_2022_realistic_v5-v2/",
		],
		"TTto2L2Nu_Hdamp418" : [ 
			"/store/mc/Run3Summer22NanoAODv12/TTto2L2Nu_Hdamp-418_TuneCP5_13p6TeV_powheg-pythia8/NANOAODSIM/130X_mcRun3_2022_realistic_v5-v2/",
		],
		"TTto2L2Nu_TuneCP5Up" : [ 
			"/store/mc/Run3Summer22NanoAODv12/TTto2L2Nu_TuneCP5Up_13p6TeV_powheg-pythia8/NANOAODSIM/130X_mcRun3_2022_realistic_v5-v2/",
		],
		"TTto2L2Nu_TuneCP5Down" : [ 
			"/store/mc/Run3Summer22NanoAODv12/TTto2L2Nu_TuneCP5Down_13p6TeV_powheg-pythia8/NANOAODSIM/130X_mcRun3_2022_realistic_v5-v2/",
		],
		"TTto2L2Nu_ERDOn" : [ 
			"/store/mc/Run3Summer22NanoAODv12/TTto2L2Nu_TuneCP5_ERDOn_13p6TeV_powheg-pythia8/NANOAODSIM/130X_mcRun3_2022_realistic_v5-v2/",
		],
	},
	"2022EE": {
		"TTto2L2Nu_mtop1695" : [ 
			"/store/mc/Run3Summer22EENanoAODv12/TTto2L2Nu_MT-169p5_TuneCP5_13p6TeV_powheg-pythia8/NANOAODSIM/130X_mcRun3_2022_realistic_postEE_v6-v2/",
		],
		"TTto2L2Nu_mtop1755" : [ 
			"/store/mc/Run3Summer22EENanoAODv12/TTto2L2Nu_MT-175p5_TuneCP5_13p6TeV_powheg-pythia8/NANOAODSIM/130X_mcRun3_2022_realistic_postEE_v6-v2/",
		],
		"TTto2L2Nu_TuneCP5Up" : [ 
			"/store/mc/Run3Summer22EENanoAODv12/TTto2L2Nu_TuneCP5Up_13p6TeV_powheg-pythia8/NANOAODSIM/130X_mcRun3_2022_realistic_postEE_v6-v2/",
		],
		"TTto2L2Nu_TuneCP5Down" : [ 
			"/store/mc/Run3Summer22EENanoAODv12/TTto2L2Nu_TuneCP5Down_13p6TeV_powheg-pythia8/NANOAODSIM/130X_mcRun3_2022_realistic_postEE_v6-v2/",
		],
		"TTto2L2Nu_ERDOn" : [ 
			"/store/mc/Run3Summer22EENanoAODv12/TTto2L2Nu_TuneCP5_ERDOn_13p6TeV_powheg-pythia8/NANOAODSIM/130X_mcRun3_2022_realistic_postEE_v6-v2/",
		],
	}
}